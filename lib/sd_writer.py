import os
from machine import SD
import led_controller
class Sd_writer:

    gpx_session = '<name>{}</name>'
    gpx_trkseg_start = '<trkseg>'
    gpx_end = '''</trkseg>
    </trk>
    </gpx>
    '''
    
    gpx_sentence = '''<trkpt lat="{}" lon="{}">
        <ele>{}</ele>
        <time>{}</time>
        <pdop>{}</pdop>
    </trkpt>'''

    def __init__(self, file led):
        self.led = led
        self.led.flash(self.led.LIGHT_CYAN, flashtime=0.5)
        self.file = file
        sd = SD()
        os.mount(sd, '/sd')
        self.f = open('/sd/sessions/' + self.file, 'a')
        self.f.write(self.gpx_start())
        self.led.flash(self.led.LIGHT_GREEN, flashtime=1)

    def gpx_start(self):
        gpx_start = '''<?xml version="1.0" encoding="utf-8" standalone="yes"?>
        <gpx version="1.1" creator="Locus Map, Android"
        xmlns="http://www.topografix.com/GPX/1/1"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"
        xmlns:gpx_style="http://www.topografix.com/GPX/gpx_style/0/2"
        xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
        xmlns:gpxtrkx="http://www.garmin.com/xmlschemas/TrackStatsExtension/v1"
        xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v2">
        <metadata>
        <desc>Created with Wipy + Pytrack!</desc>
        </metadata>
        <trk>    
        '''
        return gpx_start

    def close(self):
        self.f.close()
    
    def open_r(self):
        self.f = open('/sd/sessions/' + self.file, 'r')

    def write_sentence(self, lat, lon, ele, time, pdop):
        self.f.write(self.gpx_sentence.format(lat, lon, ele, time, pdop))

    def init_session(self, session):
        self.f.write(self.gpx_session.format(session))
        self.f.write(self.gpx_trkseg_start)

    def end_session(self):
        self.f.write(self.gpx_end)
        self.f.close()

    def read_all(self):
        self.f.close()
        self.f = open('/sd/sessions/' + self.file, 'r')
        return self.f.readall()