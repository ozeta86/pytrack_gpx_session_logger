"Gps controller"
import pycom
import time
import _thread
import led_controller

class GPSCTRL:
    def __init__(self, led, microgps):
        self.lock = _thread.allocate_lock()
        self.stop_flag = False

    def stop(self):
        self.lock.acquire()
        self.stop_flag = True
        self.lock.release()

        def start(self):
            self.stop_heartbeat()
            self.lock.acquire()
            while (self.stop_flag is False):
                self.lock.release()
                pycom.rgbled(0)
                time.sleep(1)
                pycom.rgbled(self.get_color())
                time.sleep(0.1)
                print("sleeping!")
                self.lock.acquire()
            self.lock.release()
            self.stop_heartbeat()

    
    def gps_recording_start(led):
        led.flash(led.LIGHT_BLUE, sleeptime=1)