"Led heartbeat controller"

import pycom
import time
import _thread
class LedController:

        CYAN_FC = 0x00ffcc
        ORANGE = 0xFFA500
        CYAN = 0x00ffff
        RED = 0xff0000
        BLUE = 0x0000ff
        RED = 0x00ff00
        LIGHT_CYAN = 0x001111
        LIGHT_RED = 0x110000
        LIGHT_BLUE = 0x000011
        LIGHT_GREEN = 0x001100
        LIGHT_ORANGE = 0x332100

        def __init__(self):
            self.lock = _thread.allocate_lock()
            self.continue_loop = True

        def start_default_heartbeat(self):
            pycom.heartbeat(True)
            return
        
        def stop_heartbeat(self):
            pycom.heartbeat(False)
            return

        def start(self, color, flashtime, sleeptime):
            self.stop_heartbeat()
            self.lock.acquire()
            while (self.continue_loop is True):
                self.lock.release()
                pycom.rgbled(0)
                time.sleep(flashtime)
                pycom.rgbled(color)
                time.sleep(sleeptime)
                self.lock.acquire()
            self.lock.release()
            self.stop_heartbeat()

        def stop(self):
            self.lock.acquire()
            self.continue_loop = False
            self.lock.release()

        def heartbeat(self, color, flashtime=0.1, sleeptime=1):
            self.stop()
            _thread.start_new_thread(self.start, (color, flashtime, sleeptime))

