from machine import Timer
import time
import gc
import binascii

class L76GNSS:

    GPS_I2CADDR = const(0x10)

    def __init__(self, pytrack=None, sda='P22', scl='P21', timeout=None):
        if pytrack is not None:
            self.i2c = pytrack.i2c
        else:
            from machine import I2C
            self.i2c = I2C(0, mode=I2C.MASTER, pins=(sda, scl))

        self.reg = bytearray(1)
        self.i2c.writeto(GPS_I2CADDR, self.reg)

    def _read(self):
        self.reg = self.i2c.readfrom(GPS_I2CADDR, 64)
        return self.reg

    def raw(self, debug=False):
        # time.sleep(0.1)
        str = self._read().lstrip(b'\r\n').rstrip(b'\r\n')
        str.lstrip(b'\n\n').rstrip(b'\n\n')
        return str
