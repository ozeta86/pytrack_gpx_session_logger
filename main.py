import gc
import led_controller
import time
import _thread
from L76GNSS import L76GNSS
from micropyGPS import MicropyGPS
from machine import Pin
#from gps_controller import GPSCTRL
#from sd_writer import Sd_writer

#TODO:
#heartbeat non funziona durante acquisizione

def loop(arg):
    """Loop."""
    init = False
    led.heartbeat(led.LIGHT_ORANGE, 0.3, 0.7)
    while (init is False):
        sentence = l76.raw().decode()
        for x in sentence:
            result = gps.update(x)
            if result == 'GNGLL' or result == 'GPGLL' and gps.latitude[0] != 0:
                init = True
            if result == 'GNGLL' or result == 'GPGLL':
                print("@ {} # number of sat: {}".format(gps.timestamp, len(gps.satellites_visible()))
        gc.collect()
    sdw = start_write_session(gps)
    led.heartbeat(led.LIGHT_CYAN, 0.1, 1)
    stop_lock.acquire()
    while (continue_loop is False):
        stop_lock.release()
        #led.start()
        sentence = l76.raw().decode()
        for x in sentence:
            result = gps.update(x)
            #if result in location_sentences:
            if result == 'GNGLL' or result == 'GPGLL':
                sdw.write_sentence(gps.latitude[0], gps.longitude[0], gps.altitude, gps.timestamp, gps.pdop)
                print("@ {} # number of sat: {}".format(gps.timestamp, len(gps.satellites_visible()))
        gc.collect()
        stop_lock.acquire()
    stop_lock.release()
    #led.stop()

def start_write_session(gps):
    tstamp = gps.timestamp # (8, 18, 36.0)
    date = gps.date # (22, 9, 05)
    session = '{}-{}-{}-{}-{}-{}.gpx'.format(date[2], date[1], date[0],tstamp[2], tstamp[1], int(tstamp[0]))
    sdw = None
    sdw = Sd_writer(session)
    sdw.init_session('session')
    return sdw

def init_gps():
    l76 = L76GNSS(timeout=60)
    gps = MicropyGPS(local_offset=1, location_formatting='dd')
    return (gps, l76)

def init_pin(handler, continue_loop):
    p_in = Pin('P13', mode=Pin.IN, pull=Pin.PULL_UP)
    p_in.callback(Pin.IRQ_RISING, handler)

def pin_handler(arg):
    if continue_loop is False:
        print('Recording started')
        with stop_lock:
            continue_loop = True
            _thread.start_new_thread(loop, (0))
    else:
        print('Recording stopped')
        with stop_lock:
    
            continue_loop = False

gc.enable()
stop_lock = _thread.allocate_lock()
continue_loop = False

gps, l76 = init_gps()
led = led_controller.LedController()
init_pin(pin_handler, continue_loop)
print('SERVICE STARTED!')
location_sentences = ( 'GPGGA', 'GPGLL')
location_sentences_all = ('GPRMC', 'GPGGA', 'GPGLL')
